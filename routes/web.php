<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {return view('welcome');});

Route::get('HelloWorld',function () {return view('hello');})->name('hello');
Route::get('Contoh1/{angka1}/{angka2}','pertemuan1\LatihanController@index')->name('pert1');
Route::get('Contoh2/operasi/{opsi}/{angka1}/{angka2}',
	'pertemuan2\Contoh1Controller@index')->name('cth1');

Route::get('Contoh2/{opsi}/{angka1}/{angka2}','pertemuan2\Contoh1Controller@index')->name('pert2');
Route::get('Contoh3','pertemuan2\Contoh2Controller@home')->name('tugas');
Route::get('Contoh3/{opsi}/{a}/{b}', 'pertemuan2\Contoh2Controller@index')->name('aritmatika');


// Route::get('/pert2/{opsi}/{angka1}/{angka2}', 'pertemuan2\Contoh1Controller@index');
// Route::get('/pertemuan2', 'pertemuan2\Contoh2Controller@home');

# routing untuk halaman list projects
// Route::get('/{$opsi}', function () {
//   return view('pertemuan2.contoh2');
// });

//Route::get('profile/{id}', 'somecontroller@somefunction')->name('profile');