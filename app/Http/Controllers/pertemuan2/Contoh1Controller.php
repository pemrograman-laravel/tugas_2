<?php

namespace App\Http\Controllers\pertemuan2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Contoh1Controller extends Controller
{
    public function index($opsi, $angka1, $angka2){
    	return view('pertemuan2.contoh1',['opsi' => $opsi , 'a' => $angka1, 'b' => $angka2]);
    }
    public function index1($opsi, $angka1, $angka2){
    	return view('pertemuan2.contoh1',['opsi' => $opsi , 'a' => $angka1, 'b' => $angka2]);
    }
    public function tampil($opsi, $angka1, $angka2){
    	switch ($opsi) {
		  case 'tambah': return "$angka1 + $angka2 = ".($angka1+$angka2);break;
		  case 'kurang': return "$angka1 - $angka2 = ".($angka1-$angka2);break;
		  case 'bagi': return "$angka1 / $angka2 = ".($angka1/$angka2);break;
		  case 'kali': return "$angka1 * $angka2 = ".($angka1*$angka2);break;
		  
		  default:return "Opsi tidak dikenal"; break;
		 }
    }
}
