<?php

namespace App\Http\Controllers\pertemuan2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Contoh2Controller extends Controller
{
    public function index($opsi, $angka1, $angka2){
    	return view('pertemuan2.contoh2',['opsi' => $opsi , 'a' => $angka1, 'b' => $angka2]);
    }
    public function home(){
    	return view('pertemuan2.home');
    }
}
