<?php

namespace App\Http\Controllers\pertemuan1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LatihanController extends Controller
{
    public function index($angka1, $angka2){
    	return view('pertemuan1.latihan',['a' => $angka1, 'b' => $angka2]);
    }

}
