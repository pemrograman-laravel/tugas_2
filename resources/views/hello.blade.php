@extends('layouts.master')

@section('content')
	<center>
		<h1>Hello World!</h1><hr>
		<h2 style="color: red">Hello World!</h2><hr>
		<h3 style="color: yellow">Hello World!</h3><hr>
		<h4 style="color: green">Hello World!</h4><hr>
		<h5 style="color: blue">Hello World!</h5><hr>
		<h6 style="color: purple">Hello World!</h6><hr>
	</center>
@endsection