@extends('layouts.master')

@section('page_header')
	Operasi Aritmatika
@endsection

@section('content')
<hr>
		@if($opsi=="operasi")
			<ul>
			    <li>
			    	<a href= "{{route('cth1', ['opsi' => 'tambah', 'a'=>10,'b'=>2])}}" )>Penjumlahan
			    </li>
			    <li>
			    	<a href= "{{route('cth1', ['opsi' => 'kurang', 'a'=>10,'b'=>2])}}" )>Pengurangan
			    </li>
			    <li>
			    	<a href= "{{route('cth1', ['opsi' => 'kali', 'a'=>10,'b'=>2])}}" )>Perkalian
			    </li>
			    <li><a href= "{{route('cth1', ['opsi' => 'bagi', 'a'=>10,'b'=>2])}}" )>Pembagian
			    </li>
			  </ul>

		@elseif($opsi=="tambah")
			<h1 align="center" style="color: red">Operasi Penjumlahan</h1><br><br>
			<h4 align="center">Hasil penjumlahan {{$a}} tambah  {{$b}} adalah {{$a + $b}}</h4>
		@elseif($opsi=="kurang")
			<h1 align="center" style="color: orange">Operasi Pengurangan</h1><br><br>
			<h4 align="center">Hasil pengurangan {{$a}} kurang  {{$b}} adalah {{$a - $b}}</h4>
		@elseif($opsi=="kali")
			<h1 align="center" style="color: green">Operasi Perkalian</h1><br><br>
			<h4 align="center">Hasil perkalian {{$a}} kali  {{$b}} adalah {{$a * $b}}</h4>
		@elseif($opsi=="bagi")
			<h1 align="center" style="color: purple">Operasi Pembagian</h1><br><br>
			<h4 align="center">Hasil pembagian {{$a}} bagi  {{$b}} adalah {{$a / $b}}</h4>
		@else
			<h1>Pilihan Aritmatika tidak dikenal</h1>
		@endif

@endsection