@extends('layouts.master')

@section('page_header')
	<center>Operasi Aritmatika Bilangan</center> 
@endsection

@section('content')
<h3>
  <ul>
    <li onclick="tambah()"><a href= "{{route('aritmatika', ['opsi' => 'tambah', 'a'=>10,'b'=>2])}}" )>Penjumlahan</li>
    <li><a href= "{{route('aritmatika', ['opsi' => 'kurang', 'a'=>10,'b'=>2])}}" )>Pengurangan</li>
    <li onclick="kali()"><a href= "{{route('aritmatika', ['opsi' => 'kali', 'a'=>10,'b'=>2])}}" )>Perkalian</li>
    <li><a href= "{{route('aritmatika', ['opsi' => 'bagi', 'a'=>10,'b'=>2])}}" )>Pembagian</li>
  </ul>
</h3>
@endsection
@push('scripts')
	<script src="{{asset('contoh.js')}}"></script>
	<!-- <script type="text/javascript">
		alert('HAI.......');
	</script> -->
@endpush 