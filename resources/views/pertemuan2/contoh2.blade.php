@extends('layouts.master')

@section('content')
        <!-- <H3><a href="/pertemuan2">Home</a></H3> -->
	<center>
		@if($opsi=="tambah")
			<h1 onclick="tambah()">Operasi Penjumlahan</h1>
			<h3>{{$a}} +  {{$b}} = {{$a + $b}}</h3>

    	@push('scripts')
			<script src="{{asset('contoh.js')}}"></script>
		@endpush 

		@elseif($opsi=="kurang")
			<h1 onclick="kurang()">Operasi Pengurangan</h1>
			<h3>{{$a}} -  {{$b}} = {{$a - $b}}</h3>

    	@push('scripts')
			<script src="{{asset('contoh.js')}}"></script>
		@endpush 

		@elseif($opsi=="kali")
			<h1 onclick="kali()">Operasi Perkalian</h1>
			<h3>{{$a}} x  {{$b}} = {{$a * $b}}</h3>

		@elseif($opsi=="bagi")
			<h1 onclick="bagi()">Operasi Pembagian</h1>
			<h3>{{$a}} /  {{$b}} = {{$a + $b}}</h3>
		@else
			<h1>Pilihan Aritmatika tidak dikenal</h1>
		@endif
	</center>
@endsection



